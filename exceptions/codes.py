OK = 0
UNDEFINED = -1
REQUEST_VALIDATION_ERROR = 1
RESPONSE_VALIDATION_ERROR = 2
UNAUTHORIZED = 3

error_messages = {
    OK: '',
    UNDEFINED: 'Undefined error',
    REQUEST_VALIDATION_ERROR: 'Invalid request data',
    RESPONSE_VALIDATION_ERROR: 'Response validation failed',
    UNAUTHORIZED: 'Unauthorized'
}