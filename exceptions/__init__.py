from exceptions.codes import error_messages


class AppException(Exception):

    def __init__(self, error_code: int, error_message: str = None):

        self.error_code = error_code

        if not error_message:
            error_message = error_messages.get(error_code, '')

        self.error_message = error_message

        super().__init__(error_message)

    def to_json(self):
        return {
            'status': self.error_code,
            'error_message': self.error_message
        }
