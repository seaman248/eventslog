from functools import wraps
from schematics.exceptions import DataError
from exceptions import AppException
from exceptions.codes import \
    REQUEST_VALIDATION_ERROR, RESPONSE_VALIDATION_ERROR
from sanic import response as sanic_response


def validate(RequestModel=None, ResponseModel=None):
    def decorator(f):
        @wraps(f)
        async def decoration_function(request, *args, **kwargs):

            if RequestModel:
                try:

                    request_model = RequestModel(request.json)
                    request_model.validate()
                    kwargs['request_model'] = request_model

                except DataError as e:
                    raise AppException(
                        error_code=REQUEST_VALIDATION_ERROR,
                        error_message=str(e))

            response = await f(request, *args, **kwargs)

            if ResponseModel and isinstance(response, ResponseModel):
                try:

                    response.validate()
                    response = sanic_response.json(response.to_primitive())

                except DataError as e:
                    raise AppException(
                        error_code=RESPONSE_VALIDATION_ERROR,
                        error_message=str(e))

            return response

        return decoration_function
    return decorator
