from functools import wraps

from exceptions import AppException, codes


def authorize():
    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            token = request.headers[request.app.config.AUTH_HEADER]

            if not token or token not in request.app.config.ALLOWED_APS:
                raise AppException(error_code=codes.UNAUTHORIZED)

            result = await f(request, *args, **kwargs)

            return result

        return decorated_function

    return decorator