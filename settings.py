import os

APP = {
    'HOST': 'localhost',
    'PORT': '17200'
}

REDIS = {
    'REDIS_HOST': os.environ.get('REDIS_HOST', 'localhost'),
    'REDIS_PORT': os.environ.get('REDIS_PORT', 6379),
    'REDIS_DB': int(os.environ.get('REDIS_DB', 0)),
    'REDIS_USER_CHANNEL': 'USER_CHANNEL',
    'REDIS_USER_LAST_EVENTS': 'USER_LAST_EVENTS'
}

AUTH = {
    'ALLOWED_APS': ['SHAMBALAAPP'],
    'AUTH_HEADER': 'broadcast-token'
}

EXPOSED_EVENTS = \
    ['win_bets', 'payments', 'withdrawals', 'registrations', 'bonus', 'place_bets']
