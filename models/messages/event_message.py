from schematics.types import IntType, DecimalType, StringType, BooleanType

from models.messages.message import \
    HttpResponseModel, HttpRequestModel, \
    WsRequestModel, WsResponseModel, RedisMessageModel


class EventMessageHttpRequest(HttpRequestModel):

    BETS_WIN_TYPE = 1
    BETS_SET_TYPE = 2
    BETS_LOSS_TYPE = 3
    PAYMENTS_TYPE = 4
    REGISTRATIONS_TYPE = 5
    BONUS_TYPE = 6

    EVENT_TYPES = {
        BETS_WIN_TYPE: 'bets_win',
        BETS_SET_TYPE: 'bets_set',
        BETS_LOSS_TYPE: 'bets_loss',
        PAYMENTS_TYPE: 'payments',
        REGISTRATIONS_TYPE: 'registrations',
        BONUS_TYPE: 'bonus',
    }

    event_type = IntType(required=True, choices=EVENT_TYPES.keys())
    amount = DecimalType(default=0)
    user_id = IntType(required=False)


class EventMessageHttpResponse(HttpResponseModel):
    pass


class EventMessageWsResponse(WsResponseModel, EventMessageHttpRequest):
    pass


class EventMessageRedis(RedisMessageModel, EventMessageHttpRequest):
    pass
