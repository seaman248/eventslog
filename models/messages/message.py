from json import dumps
from schematics import Model
from schematics.types import StringType, IntType, TimestampType
from exceptions import codes
from exceptions.codes import error_messages
from time import time


class BaseMessage(Model):
    timestamp = TimestampType(default=time)


class BaseRequest(BaseMessage):
    pass


class BaseResponse(BaseMessage):
    status = IntType(default=codes.OK, choices=error_messages.keys())
    error_message = StringType(default=error_messages[codes.OK])


class HttpRequestModel(BaseRequest):
    pass


class HttpResponseModel(BaseResponse):
    pass


class WsRequestModel(BaseRequest):
    pass


class WsResponseModel(BaseResponse):
    status = IntType(default=codes.OK, choices=error_messages.keys())
    error_messages = StringType(default=error_messages[codes.OK])


class RedisMessageModel(BaseMessage):

    def to_string(self):
        return dumps(self.to_primitive(), ensure_ascii=False)


class Message:
    def __init__(self, data: dict):
        self.data = data

    def to_string(self):
        return dumps(self.data, ensure_ascii=False)

    def __repr__(self):
        return f'<Message {self.data}>'