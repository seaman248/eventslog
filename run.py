import asyncio
import uvloop
import logging
from logging import config
from logging import Logger
from sanic import Sanic
from handlers.ws import ws
from handlers.http import http
from redis_connection.conn import create_connection, subscribe
from settings import REDIS, AUTH
from aioredis import create_redis
from exceptions import AppException
from exceptions.codes import UNDEFINED
from sanic import response
import os


logging.config.fileConfig('logging.conf', disable_existing_loggers=False)
log: Logger = logging.getLogger(__name__)

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

app = Sanic()

app.blueprint(ws)
app.blueprint(http)


@app.exception(AppException)
def app_exception(request, exception: AppException):
    log.error(f'App exception {exception} detected '
              f'during handling request {request}')

    return response.json(exception.to_json())


@app.exception(Exception)
def any_exception(request, exception: Exception):
    log.error(f'App exception {exception} detected during '
              f'handling rquest {request}')

    return response.json({
        'status': UNDEFINED,
        'error_message': str(exception)
    })


@app.listener('before_server_start')
async def setup_redis_connection(app, loop: asyncio.BaseEventLoop):
    app.config.update(REDIS)
    app.config.update(AUTH)

    app.redis = await create_connection(app.config)
    app.subscribe = await subscribe(app.config)
    app.pub = await create_redis(
        f'redis://{app.config.REDIS_HOST}:{app.config.REDIS_PORT}')


if __name__ == '__main__':

    if os.environ.get('MODE') == 'random_broadcast':
        from randMessages import start
        start()

    app.run(os.environ['APP_HOST'], os.environ['APP_PORT'])


