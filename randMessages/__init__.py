from models.messages.message import Message
from datetime import datetime as dt
import random
import requests
from time import sleep
import json
import settings
import os
from logging import getLogger, Logger

speed_coef = 1

next_interval = 0
next_event_type = 1

log: Logger = getLogger(__name__)


class RandomMessage(Message):

    EVENT_BONUS = 1
    EVENT_PAYMENT = 2
    EVENT_WINBET = 3
    EVENT_LOSSBET = 4
    EVENT_REGISTRATION = 5
    EVENT_WITHDRAWAL = 6

    EVENTS = {
        EVENT_BONUS: 'bonus',
        EVENT_PAYMENT: 'payment',
        EVENT_WINBET: 'win_bets',
        EVENT_REGISTRATION: 'registrations',
        EVENT_WITHDRAWAL: 'withdrawals',
        EVENT_LOSSBET: 'loss_bets'
    }

    CONTENT = {
        EVENT_BONUS: 'Бонус зачислен на счет пользователя',
        EVENT_PAYMENT: 'Пользователь произвел оплату',
        EVENT_WINBET: 'Выигрыш зачислен на счет пользователя',
        EVENT_REGISTRATION: 'Новый пользователь зарегистрирован на сайте',
        EVENT_WITHDRAWAL: 'Пользователь успешно вывел средства',
        EVENT_LOSSBET: 'Ставка пользователя оказалась неудачной'
    }

    REAL_EVENTS_PER_DAY = {
        EVENT_BONUS: 0.03,
        EVENT_REGISTRATION: 0.9,
        EVENT_PAYMENT: 0.8,
        EVENT_WITHDRAWAL: 0.26,
        EVENT_WINBET: 5.6,
        EVENT_LOSSBET: 6.2
    }

    # 0-9999, 10000-19999, ..., 90000-99999
    AMOUNT_WEIGHTS = {
        EVENT_PAYMENT: [491, 39, 1, 1, 1, 0, 0, 0, 0, 0],
        EVENT_WITHDRAWAL: [171, 8, 3, 8, 1, 0, 0, 0, 0, 0],
        EVENT_WINBET: [3796, 158, 57, 23, 10, 10, 4, 3, 3, 3],
        EVENT_LOSSBET: [4149, 219, 68, 55, 2, 16, 3, 3, 1, 0]
    }

    def __init__(self, type: int = None):
        data = {
            'date': str(dt.now())
        }

        self._type = type or self._choose_type()

        data['event_type'] = self.EVENTS[self._type]
        data['content'] = self._choose_content()

        if self._type in self.AMOUNT_WEIGHTS.keys():
            data['amount'] = self._choose_amount()

        super().__init__(data=data)

    def _choose_type(self) -> int:
        su_events = sum(self.REAL_EVENTS_PER_DAY.values())

        choosed = False

        while not choosed:
            randGen = (random.randrange(0, int(su_events)) + 0.001) + \
                      random.choice([.01, .2, .3, .4, .5, .6, .7, .8, .9])

            for key, value in self.REAL_EVENTS_PER_DAY.items():
                if value >= randGen:
                    choosed = True
                    return key

    def _choose_content(self) -> str:
        return self.CONTENT[self._type]

    def _choose_amount(self) -> float:
        weights = self.AMOUNT_WEIGHTS[self._type]

        maxW = max(weights)

        randDidgit = random.randrange(0, maxW)

        counter = 0

        value: int

        for i in reversed(weights):
            if randDidgit <= i:
                value = counter
                break
            counter += 1

        result = random.randrange(0, value * 1000)

        return round(result, -2) if self._type != self.EVENT_WINBET else round(result * 1.03, 2)

    def next_event(self):
        if self._type in [self.EVENT_WINBET, self.EVENT_LOSSBET] and \
                random.randrange(0, 99) > 50:

            return self.__class__(type=self._type)

        next_event_type: int = self._choose_type()

        next_event = self.__class__(type=next_event_type)

        return next_event

    def next_interval(self):
        if self._type in [self.EVENT_LOSSBET, self.EVENT_WINBET]:
            return random.randrange(1, 2)

        return random.randrange(7, 20)


def send_message(message: RandomMessage):
    log.info(f'Send random message {message}')

    url = f'http://{os.environ["APP_HOST"]}:{os.environ["APP_PORT"]}/events/broadcast'

    requests.post(url, json={
        'token': settings.AUTH['ALLOWED_APS'][0],
        'message': json.loads(message.to_string())
    })


def start():
    nextMessage: RandomMessage = RandomMessage()
    nextInterval = nextMessage.next_interval()

    while True:
        send_message(nextMessage)
        sleep(nextInterval)

        nextMessage = nextMessage.next_event()
        nextInterval = nextMessage.next_interval()


if __name__ == '__main__':
    start()
