import aioredis
import asyncio
from aioredis import RedisConnection, Channel
from aioredis.commands import ListCommandsMixin
from models.messages.message import Message
from models.messages.event_message import EventMessageWsResponse, \
    EventMessageRedis

import logging

log: logging.Logger = logging.getLogger(__name__)


async def create_connection(conf) -> RedisConnection:
    connected: bool = False
    connection: RedisConnection

    while not connected:
        try:
            log.info(
                f'Try connect to redis {conf.REDIS_HOST}:{conf.REDIS_PORT}')

            connection = await aioredis.create_connection(
                (conf.REDIS_HOST, conf.REDIS_PORT), db=conf.REDIS_DB)

            connected = True

        except OSError as e:
            log.error(f'Try reconnect to redis after 5 seconds')
            await asyncio.sleep(5)

    if connected:
        log.info('Connection to redis has been established')

    connection.__class__ = type('RedisConnection',
                                (RedisConnection, ListCommandsMixin), {})

    return connection


async def subscribe(conf):
    new_connection = await create_connection(conf)
    await new_connection.execute_pubsub('subscribe', conf.REDIS_USER_CHANNEL)
    return new_connection


async def get_message(app) -> Message:
    subscribe = app.subscribe
    channel_name: str = app.config.REDIS_USER_CHANNEL

    channel: Channel = subscribe.pubsub_channels[channel_name]

    message: dict = await channel.get_json()

    log.info(f'Get new message {message} from redis')

    return Message(data=message)


async def latest_messages(app) -> []:
    log.info('Get latest messages from redis')

    messages = await app.redis\
        .lrange(app.config.REDIS_USER_LAST_EVENTS, 0, 4, encoding='UTF-8')

    return [Message(data=mes) for mes in messages]


async def publish_event(app, message: EventMessageRedis):
    ch = app.config.REDIS_USER_CHANNEL
    list = app.config.REDIS_USER_LAST_EVENTS

    log.info(f'Message {message.to_primitive()} received')

    pub = app.pub.publish_json(ch, message.to_primitive())
    ad1 = app.pub.lpush(list, message.to_string())
    ad2 = app.pub.ltrim(list, 0, 4)

    await asyncio.gather(pub, ad1, ad2)
