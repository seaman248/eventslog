from sanic import Blueprint, request
import asyncio
from redis_connection.conn import get_message, latest_messages

ws = Blueprint('wsFeed')


@ws.websocket('/events_feed')
async def events_feed(request: request, ws):
    msgs = await latest_messages(request.app)

    for msg in msgs:
        await ws.send(msg.to_string())

    while True:
        message = await get_message(request.app)
        await ws.send(message.to_string())
        await asyncio.sleep(1)
