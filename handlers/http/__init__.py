from sanic import Blueprint, request
import asyncio
import logging

from decorators.authorize import authorize
from decorators.validate import validate

from models.messages.event_message import \
    EventMessageHttpRequest, EventMessageHttpResponse, EventMessageRedis

from redis_connection.conn import publish_event

log: logging.Logger = logging.getLogger(__name__)

http = Blueprint('http')


@http.route('/events/broadcast', methods=['POST'])
@authorize()
@validate(EventMessageHttpRequest, EventMessageHttpResponse)
async def broadcast(request: request,
                    request_model: EventMessageHttpRequest=None):

    message_redis = EventMessageRedis(request_model.to_primitive())

    asyncio.ensure_future(publish_event(request.app, message_redis))

    return EventMessageHttpResponse()

